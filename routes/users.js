var express = require('express');
var router = express.Router();
var db = require('../database/connection');
var usersConfiguration = require('../configuration/usersConfiguration.json');

router.get('/getUsers', function (req, res, next) {
    const query = usersConfiguration.getUsers;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

router.post('/getUser', function (req, res, next) {
    const query = usersConfiguration.getUser;
    const username = req.body.username;
    const password = req.body.password;
    db.query(query, [username, password], function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

module.exports = router;
