var express = require('express');
var router = express.Router();
var package = require('../package.json');

const server = {
    nome: package.name,
    version: package.version,
    date: Date.now()
};

router.get('/', function (req, res, next) {
    return res.status(200).json(server);
});

module.exports = router;
