var express = require('express');
var router = express.Router();
var db = require('../database/connection');
var authorsConfiguration = require('../configuration/authorsConfiguration.json');

router.get('/getAuthors', function (req, res, next) {
    const query = authorsConfiguration.getAllAuthors;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

module.exports = router;
