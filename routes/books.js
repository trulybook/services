var express = require('express');
var router = express.Router();
var db = require('../database/connection');
var booksConfiguration = require('../configuration/booksConfiguration.json');

router.get('/getBooks', function (req, res, next) {
    const query = booksConfiguration.getBooks;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

router.get('/getContentVideo', function (req, res, next) {
    const query = booksConfiguration.getContentVideo;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

router.get('/getContentAudio', function (req, res, next) {
    const query = booksConfiguration.getContentAudio;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

router.get('/getBooksByCategory/:category', function (req, res, next) {
    const query = booksConfiguration.getBooksByCategory;
    const id_category = req.params.category;
    db.query(query, [id_category], function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

router.get('/getBooksByAuthor/:author', function (req, res, next) {
    const query = booksConfiguration.getBooksByAuthor;
    const id_author = req.params.author;
    const author = {};
    db.query(query, [id_author], function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            author.id_author = rows[0].id_author;
            author.name = rows[0].name;
            author.surname = rows[0].surname;
            author.motto = rows[0].motto;
            author.avatar = rows[0].avatar;
            return res.status(200).json({author: author, books: rows});
        }
    });
});

router.get('/getContentReadByBook/:book', function (req, res, next) {
    const query = booksConfiguration.getContentReadByBook;
    const id_book = req.params.book;
    const book = {};
    db.query(query, [id_book], function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            book.id_book = rows[0].id_book;
            book.title = rows[0].title;
            book.img = rows[0].img;
            book.id_category = rows[0].id_category;
            book.category = rows[0].category;
            book.id_author = rows[0].id_author;
            book.name = rows[0].name;
            book.surname = rows[0].surname;
            return res.status(200).json({book: book, libro: rows});
        }
    });
});

router.get('/getContentVideoByBook/:book', function (req, res, next) {
    const query = booksConfiguration.getContentVideoByBook;
    const id_book = req.params.book;
    const book = {};
    db.query(query, [id_book], function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            book.id_book = rows[0].id_book;
            book.title = rows[0].title;
            book.img = rows[0].img;
            book.id_category = rows[0].id_category;
            book.category = rows[0].category;
            book.id_author = rows[0].id_author;
            book.name = rows[0].name;
            book.surname = rows[0].surname;
            return res.status(200).json({book: book, video: rows});
        }
    });
});

router.get('/getContentAudioByBook/:book', function (req, res, next) {
    const query = booksConfiguration.getContentAudioByBook;
    const id_book = req.params.book;
    const book = {};
    db.query(query, [id_book], function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            book.id_book = rows[0].id_book;
            book.title = rows[0].title;
            book.img = rows[0].img;
            book.id_category = rows[0].id_category;
            book.category = rows[0].category;
            book.id_author = rows[0].id_author;
            book.name = rows[0].name;
            book.surname = rows[0].surname;
            return res.status(200).json({book: book, audio: rows});
        }
    });
});

router.get('/getRelatedBooks', function (req, res, next) {
    const query = booksConfiguration.getRelatedBooks;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

module.exports = router;
