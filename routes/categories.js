var express = require('express');
var router = express.Router();
var db = require('../database/connection');
var categoriesConfiguration = require('../configuration/categoriesConfiguration.json');

router.get('/getCategories', function (req, res, next) {
    const query = categoriesConfiguration.getCategories;
    db.query(query, function (err, rows) {
        if (err) {
            return res.status(500).send(err);
        } else {
            return res.status(200).json(rows);
        }
    });
});

module.exports = router;
