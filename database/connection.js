var mysql = require('mysql');
var db = require('./config.json');
var connection = mysql.createConnection(db);

connection.connect(function (err) {
    if (err) {
        console.error(err.stack);
        return;
    }
    console.log('Connected!');
});

module.exports = connection;
